#This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard
from trytond.pool import Pool


class AddressReportAsk(ModelView):
    'Address Report Ask'
    _name = 'party.address.report.ask'
    _description = __doc__
    from_zip = fields.Integer('From zip')
    thru_zip = fields.Integer('Thru zip')
    categories = fields.Many2Many('party.party-party.category',
        'party', 'category', 'Categories')

AddressReportAsk()


class AddressReport(Wizard):
    'Open Journal'
    _name = 'party.address.report'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'party.address.report.ask',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('search', 'Search', 'tryton-ok', True),
                ],
            },
        },
        'search': {
            'result': {
                'type': 'action',
                'action': '_action_open_address_report',
                'state': 'end',
            },
        },
    }

    def _get_childs(self, categories):
        category_obj = Pool().get('party.category')
        categories = category_obj.browse(categories)
        ids = []
        for category in categories:
            ids.append(category.id)
            if category.childs:
                child_ids = []
                for child in category.childs:
                    child_ids.append(child.id)
                ids.extend(self._get_childs(child_ids))
        return ids

    def _action_open_address_report(self, data):
        pool = Pool()
        party_address_obj = pool.get('party.address')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')
        from_zip = data['form']['from_zip']
        thru_zip = data['form']['thru_zip']
        categories = data['form']['categories'][0][1]

        from_zip = from_zip > 1000 and from_zip or 1000
        thru_zip = thru_zip > 1000 and thru_zip or 1000
        if not (from_zip==1000 and thru_zip==1000):
            zips = [x.zfill(5) for x in 
                map(str, range(from_zip, thru_zip + 1, 1))]
        else:
            zips = []

        address_ids = []
        args = []

        if len(categories) > 0:
            cat_ids = self._get_childs(categories)
            args.append(('party.categories', 'in', cat_ids))
        if len(zips) > 0:
            args.append(('zip', 'in', zips))

        address_ids.extend(party_address_obj.search(args))

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_address_form'),
            ('module', '=', 'party'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['domain'] = str([
            ('id', 'in', address_ids),
            ])
        return res

AddressReport()

